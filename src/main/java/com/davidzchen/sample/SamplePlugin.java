package com.davidzchen.sample;

import java.util.ArrayList;
import java.util.List;

public class SamplePlugin {
	private ArrayList<String> fooList;

	public SamplePlugin() {
		this.fooList = new ArrayList<String>();
	}

	public void add(String str) {
		fooList.add(str);
	}

	public List<String> getFooList() {
		return fooList;
	}
}

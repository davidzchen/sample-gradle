Sample Java Project Using Gradle
================================

Run the following to build:

    $ gradle build

Jars will be in the following directory:

    ./build/libs
